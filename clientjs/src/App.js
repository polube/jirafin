import React, { useState, useEffect } from 'react';
import './index.css'

function App() {
  const [allIssues, setIssues] = useState();
  const [allUsers, setUsers] = useState();

  useEffect(() => {
    window.AP.request('/rest/api/3/search?jql=project%20%3D%20MAT', {
      success: function (response, req) {
        uploadIssues(response)
      }
    })
    window.AP.request('/rest/api/2/user/assignable/search?project=MAT', {
      success: function (response, req) {
        uploadUsers(response)
      }
    })
  }, []);

  const uploadIssues = (rs) => {
    setIssues(JSON.parse(rs));
  }
  const uploadUsers = (rs) => {
    setUsers(JSON.parse(rs));
  }

  // window.AP.context.getToken(function (token) {
  //   console.log("JWT token string", token);
  // });


  if (allIssues) {

    return (
      <>


        {/* {/* <button onClick={() => console.log(allIssues)}>Click</button> */}
        <button onClick={() => console.log(allIssues)}>issues</button>
        <button onClick={() => console.log(allUsers)}>users</button>
        {/* <button onClick={() => console.log(count)}>count</button> */}


        <table>
          <thead>
            <th class="nope"></th>
            {allUsers.map(user => {
              return (<>
                <th>{user.displayName}</th>

              </>
              )
            })}
          </thead>

          <tbody>

            {allIssues.issues.map(issue => {
              return (
                <tr>
                  <td class="firstTd">{issue.fields.summary}</td>
                  {allUsers.map(user => {
                        let out = '';

                    
                    if (issue.fields.customfield_10041 !== null && issue.fields.customfield_10041[0].accountId === user.accountId) {
                      // uploadCount('R');
                      out = out + 'R'

                    }
                    if (issue.fields.customfield_10040 !== null && issue.fields.customfield_10040[0].accountId === user.accountId) {
                      // uploadCount('A');
                      out = out + 'A'
                    }
                    if (issue.fields.customfield_10034 !== null && issue.fields.customfield_10034[0].accountId === user.accountId) {
                      // uploadCount('C');
                      out = out + 'C'
                    }
                    if (issue.fields.customfield_10039 !== null && issue.fields.customfield_10039[0].accountId === user.accountId) {
                      // uploadCount('I');
                      out = out + 'I'
                    }
                    return (
                      <td class="field">{out}</td>
                    )
                  })}
                </tr>
              )


            })}

          </tbody>

        </table>




      </>
    )


  } else {
    return (<p>DOWNLOADING!</p>)
  }

}

export default App;
